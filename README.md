# Projet UE4 : Module Entrepôt et fouille de données

**Auteurs :** Aurélien Chick et Thibault Dhalluin

### Objectifs du projet :

L'objectif principal de ce projet est d'analyser la corrélation entre exposition environnementales et la répartition des patients atteints de la maladie XXX.

Pour effectuer cette analyse, nous construirons un mini-entrepôt de données environnementales au format i2b2.

Afin de faciliter l'interprétation de notre analyse, nous construirons une application Shiny présentant notre recherche ainsi que des cartes intéractives pour visualiser données.

### Sources de données :

Nous disposons pour ce projet de plusieurs sources de données :

* Données sur les déchets radioactifs (https://inventaire.andra.fr/les-donnees/linventaire-national-en-open-data) par code INSEE (https://data.opendatasoft.com/explore/dataset/code-postal-code-insee-2015%40public/information/)

* Polluants par établissement (http://www.georisques.gouv.fr/dossiers/irep/form-etablissement#/) avec leur localisation (https://public.opendatasoft.com/explore/dataset/registre-francais-des-emission-polluantes-etablissements/table/)

* Radon (https://www.data.gouv.fr/fr/datasets/connaitre-le-potentiel-radon-de-ma-commune/)

* Pesticides eaux souterraines (https://www.data.gouv.fr/fr/datasets/pesticides-dans-les-eaux-souterraines/#_)
